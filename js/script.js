document.querySelector("#suma").setAttribute("onclick", "calcular('s')");
document.querySelector("#resta").setAttribute("onclick", "calcular('r')");
document
    .querySelector("#multiplicacion")
    .setAttribute("onclick", "calcular('m')");
document.querySelector("#division").setAttribute("onclick", "calcular('d')");

let resultado = document.querySelector("#resultado");

// function calcular(tipo_operacion) {
//     let n1 = parseFloat(document.querySelector("#n1").value);
//     let n2 = parseFloat(document.querySelector("#n2").value);

//     switch (tipo_operacion) {
//         case "s":
//             resultado.value = (n1 + n2).toFixed(2);
//             break;
//         case "r":
//             resultado.value = (n1 - n2).toFixed(2);
//             break;
//         case "m":
//             resultado.value = (n1 * n2).toFixed(2);
//             break;
//         case "d":
//             resultado.value = (n1 / n2).toFixed(2);
//             break;
//         default:
//             break;
//     }
// }
// PRACTICAMOS CON IF Y CON IF / ELSE
// function calcular(tipo_operacion) {
//     let n1 = parseFloat(document.querySelector("#n1").value);
//     let n2 = parseFloat(document.querySelector("#n2").value);

//     //DESARROLLAMOS UTLIZANDO IFs
//     if (tipo_operacion === "s") {
//         console.log("suma");
//         resultado.value = (n1 + n2).toFixed(2);
//         return;
//     }
//     if (tipo_operacion === "r") {
//         console.log("resta");
//         resultado.value = (n1 - n2).toFixed(2);
//         return;
//     }
//     if (tipo_operacion === "m") {
//         console.log("multiplicación");
//         resultado.value = (n1 * n2).toFixed(2);
//     } else {
//         console.log("división");
//         if (n2 === 0) {
//             resultado.value = "No se puede dividir por 0";
//         } else {
//             resultado.value = (n1 / n2).toFixed(2);
//         }
//     }
// }
// PRACTICAMOS CON IF / ELSE-IF
function calcular(tipo_operacion) {
    let n1 = parseFloat(document.querySelector("#n1").value);
    let n2 = parseFloat(document.querySelector("#n2").value);

    //DESARROLLAMOS UTLIZANDO IFs
    if (tipo_operacion === "s") {
        console.log("suma");
        resultado.value = (n1 + n2).toFixed(2);
    } else if (tipo_operacion === "r") {
        console.log("resta");
        resultado.value = (n1 - n2).toFixed(2);
    } else if (tipo_operacion === "m") {
        console.log("multiplicación");
        resultado.value = (n1 * n2).toFixed(2);
    } else {
        console.log("división");
        if (n2 === 0) {
            resultado.value = "No se puede dividir por 0";
        } else {
            resultado.value = (n1 / n2).toFixed(2);
        }
    }
}
